(function($){
  "use strict";

  if ( $('html').hasClass('lte-ie8') ) {
    $('.outdated-browser').show();
  }

  // Loading
  $('.loading').addClass('hide');
  function toggleLoading() {
    var action = $('.loading').hasClass('hide') ? 'show' : 'hide';
    if (action === 'show') {
      $('body').css('overflow', 'hidden');
      var invertedAction = 'hide';
    } else {
      $('body').css('overflow', 'visible');
      var invertedAction = 'show';
    }
    $('.loading').addClass(action).removeClass(invertedAction);
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#signup-form').submit(function() {
    console.log('asdfas');
    return;
    var $this = $(this);

    $.ajax({
      url: $this.attr('action'),
      method: 'POST',
      data: $this.serialize()
    }).done(function(response){
      if(response.success) {
        $('#call-me-modal').modal('hide');
        return sweetAlert(response.message, '', 'success');
      }
      return sweetAlert('Oups!', response.message, 'error');
    });

    return false;
  });


})(jQuery);
