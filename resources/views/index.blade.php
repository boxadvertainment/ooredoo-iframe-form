<!doctype html>
<!--[if lte IE 8]>     <html class="no-js lte-ie8" lang="{{ App::getLocale() }}"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="favicon.ico" />

    <title>Ooredoo</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="http://ooredoo.tn/sites/default/files/css/css_mxegUIb0j1xgo_PmXp4MKLl-aJKFn-JGwWc28yrGDLk.css">
    <style>
        .modal-content{
            background-color: #eee;
            color: #666;
            border-radius: 10px;
            overflow: hidden;
        }
        .modal-content .close{
            position: absolute;
            right: 24px;
            top: 21px;
        }
    </style>
</head>
<body>

    <div style="width: 170px">
        {{--<a class="submit_divrecherche lightbox-processed" href="/home?source=pro&offre=3g">Cette offre vous intéresse ?</a>--}}
        <a class="submit_divrecherche lightbox-processed" data-toggle="modal" data-source="pro" data-offre="3g" data-target="#myModal" id="openmodalconatct" >Cette offre vous intéresse ?</a>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <iframe src="" frameborder="0" class="embed-responsive-item" id="iframe" width="100%" height="600"></iframe>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="{{ asset('js/modal.js') }}"></script>
    <script type="text/javascript">
        $('#openmodalconatct').on('click', function(e) {
            var source = "./home?source="+$(this).attr('data-source')+"&offre="+$(this).attr('data-offre');
            //var source = "./home";
            //console.log(source);
            $('#myModal iframe').attr({'src':source});
            //$('#myModal').modal({show:true})
        });
    </script>
</body>
</html>


