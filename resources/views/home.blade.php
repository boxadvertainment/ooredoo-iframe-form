@extends('layout')

@section('class', 'home2')

@section('content')
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-title">Cette offre vous intéresse ?</span>
                </div>
                @if(Session::has('message'))
                    <div class="sucess-message">Nous vous contacterons dans les plus brefs délais</div>
                @else
                <form id="signup-form" action="{{ action('AppController@signup') }}" method="post">
                    <div class="modal-body clearfix">
                        <div class="col-xs-10 col-xs-offset-1">

                            {!! csrf_field() !!}

                            <p>Renseignez ces champs et nous vous contacterons pour plus d’infos</p>

                            <div class="form-group name">
                                <label class="control-label">Nom et prénom<span class="text-red">*</span> :</label>
                                <div class="inputs">
                                    <input type="text" class="form-control" name="name" id="name" maxlength="30"  placeholder="Nom et prénom *" title="Ce champ est obligatoire" required>
                                </div>
                            </div>

                            <div class="form-group name">
                                <label class="control-label">Société :</label>
                                <div class="inputs">
                                    <input type="text" class="form-control" name="company" id="company" maxlength="30" >
                                </div>
                            </div>

                            <div class="form-group name">
                                <label class="control-label">Email<span class="text-red">*</span> :</label>
                                <div class="inputs">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email *" required  title="Email incorrect">
                                </div>
                            </div>

                            <div class="form-group phone">
                                <label class=" control-label">N° de contact<span class="text-red">*</span> :</label>
                                <div class="inputs">
                                    <div class="input-group">
                                        <div class="input-group-addon"><b>+216</b></div>
                                        <input type="tel" class="form-control" name="phone" id="phone" maxlength="8" placeholder="Téléphone *" maxlength="8" required pattern="^[0-9]{8}$" title="Téléphone incorrect">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group name">
                                <label class="control-label">Votre demande :</label>
                                <div class="inputs">
                                    <select class="form-control" name="demande" id="demande" title="votre demande incorrect" required>
                                        <option value="">Choisir une demande</option>
                                        <option value="Je veux une démo">Je veux une démo</option>
                                        <option value="Je veux acheter">Je veux acheter</option>
                                        <option value="Une configuration personnalisée">Une configuration personnalisée</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group name">
                                <label class="control-label">Description:</label>
                                <div class="inputs">
                                    <textarea rows="4" class="form-control" name="description" id="description"></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="source" id="source" value="{{ Input::get('source') }}" >
                            <input type="hidden" name="offre" id="offre" value="{{ Input::get('offre') }}" >

                            <button type="submit" class="btn btn-primary">Valider</button>

                        </div>
                    </div>
                </form>
                @endif
@stop
@section('styles')
    <style>
        @font-face {
            font-family: ooredoo;
            src:
                    url('./fonts/ooredoo-regular-webfont.woff2') format('woff2'),
                    url('./fonts/ooredoo-regular-webfont.woff') format('woff');
        }

        @font-face {
            font-family: ooredoo;
            src:
                    url('../fonts/ooredoo-bold-webfont.woff2') format('woff2'),
                    url('../fonts/ooredoo-bold-webfont.woff') format('woff');
            font-weight: bold;
        }

        @font-face {
            font-family: ooredoo;
            src:
                    url('../fonts/ooredoo-light-webfont.woff2') format('woff2'),
                    url('../fonts/ooredoo-light-webfont.woff') format('woff');
            font-weight: 300;
        }
        body {
            background-color: transparent;
            font-family: ooredoo !important;
        }
        .sucess-message {
            font-size: 32px;
            text-align: center;
            padding: 330px 50px 0;
            background: url(img/check-alt.svg) center 230px no-repeat;
            background-size: 80px;
        }
        #signup-form {
            padding-top: 60px;
        }
        #signup-form .btn{
            width: 100%;
            margin: 20px 0;
            font-family: ooredoo !important;
        }

        .modal-header {
            border-bottom: transparent;
            background-color: #FFF;
            text-align: center;
            color: #CCC !important;
            font-size: 28px;
            position: fixed;
            width: 100%;
            z-index: 100;
        }
        .modal-header .close{
            position: absolute;
            right: 24px;
            top: 21px;
        }
        .modal-title {
            color: #ed1c24;
            font-size: 27px;
        }
        .form-control {
            font-family: ooredoo !important;
        }

    </style>
@endsection
@section('scripts')
    <script type="text/javascript">

        jQuery(document).ready(function($) {
            $('.close').click(function() {
                window.parent.Lightbox.end();
            });
        });

        Parsley.addMessages('fr', {
            defaultMessage: "Cette valeur semble non valide.",
            type: {
                email:        "Cette valeur n'est pas une adresse email valide.",
                url:          "Cette valeur n'est pas une URL valide.",
                number:       "Cette valeur doit être un nombre.",
                integer:      "Cette valeur doit être un entier.",
                digits:       "Cette valeur doit être numérique.",
                alphanum:     "Cette valeur doit être alphanumérique."
            },
            notblank:       "Cette valeur ne peut pas être vide.",
            required:       "Ce champ est requis.",
            pattern:        "Cette valeur semble non valide.",
            min:            "Cette valeur ne doit pas être inférieure à %s.",
            max:            "Cette valeur ne doit pas excéder %s.",
            range:          "Cette valeur doit être comprise entre %s et %s.",
            minlength:      "Cette chaîne est trop courte. Elle doit avoir au minimum %s caractères.",
            maxlength:      "Cette chaîne est trop longue. Elle doit avoir au maximum %s caractères.",
            length:         "Cette valeur doit contenir entre %s et %s caractères.",
            mincheck:       "Vous devez sélectionner au moins %s choix.",
            maxcheck:       "Vous devez sélectionner %s choix maximum.",
            check:          "Vous devez sélectionner entre %s et %s choix.",
            equalto:        "Cette valeur devrait être identique."
        });
        Parsley.setLocale('fr');
        $('#signup-form').parsley({
            successClass: 'has-success',
            errorClass: 'has-error',
            classHandler: function(field) {
                return field.$element.closest('.form-group');
            },
            errorsContainer: function(field) {
                return field.$element.closest('.form-group');
            },
            errorsWrapper: '<span class=\"help-block\"></span>',
            errorTemplate: '<span></span>'
        });

    </script>
@endsection