<!doctype html>
<!--[if lte IE 8]>     <html class="no-js lte-ie8" lang="{{ App::getLocale() }}"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title', Config::get('app.name'))</title>
    <meta name="description" content="@yield('description')">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="{{ Config::get('services.facebook.client_id') }}"/>
    <meta property="og:image" content="@yield('meta.image')"/>
    <meta property="og:title" content="@yield('meta.title')"/>
    <meta property="og:url" content="@yield('meta.url', url('/'))"/>
    <meta property="og:site_name" content="{{ Config::get('app.name') }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="@yield('meta.description')"/>

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@site">
    <meta name="twitter:title" content="@yield('meta.title')">
    <meta name="twitter:description" content="@yield('meta.description')">
    <meta name="twitter:image" content="@yield('meta.image')">

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @yield('styles')

    <!--[if lte IE 8]><script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script>
        BASE_URL    = '{{ URL::to('/') }}';
        CURRENT_URL = '{{ URL::full() }}';
        USER_AUTH   = {{ Auth::check() ? 'true' : 'false' }};
        FB_APP_ID   = '{{ Config::get('services.facebook.client_id') }}';
    </script>
</head>
<body class="@yield('class')">

    <div class="loading">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>

    <main id="app" class="main" role="main">
        @yield('content')
    </main>


    @yield('body')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('scripts')


</body>
</html>
