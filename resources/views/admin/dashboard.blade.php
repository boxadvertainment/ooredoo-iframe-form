<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Deyson Bejarano">
    <title>Admin Panel - Chkoun ?</title>
    <meta name="description" content="Bootstrap Complete User Profile Page for Bootstrap 3. Is part of the  gallery of free snippets for bootstrap css html js framework tags: page,complete">
    <meta name="keywords" content="Bootstrap, Snippet, Html, Css, page,complete">
    <link rel="author" href="https://plus.google.com/u/0/106310663276114892188/posts"/>
    <meta property="twitter:account_id" content="2433978487" />


<body>
<div id="snippetContent" style="padding-top:10px;">
    <hr>
    <div class="container">
        <div class="alert alert-info" role="alert">
            Le nombre total des inscrits : <strong>{{ $count }}</strong> <br/>
        </div>
        <div class="row">
            <div class="col-sm-10"><h1>Office in a box</h1></div>
            {{--<div class="col-sm-2"><a href="/users" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>--}}
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-12">
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Nom et prénom</th>
                        <th>Société</th>
                        <th>Email</th>
                        <th>N° de contact</th>
                        <th>source</th>
                        <th>offre</th>
                        <th>Demande</th>
                        <th>Description</th>
                        <th>Date de création</th>
                    </thead>
                    <tbody>
                    @foreach($clients as $key => $client)
                        <tr>
                            <td>{{ $client->id }}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->company }}</td>
                            <td>{{ $client->email }}</td>
                            <td>+216 {{ $client->phone }}</td>
                            <td>{{ $client->source }}</td>
                            <td>{{ $client->offre }}</td>
                            <td>{{ $client->demande }}</td>
                            <td>{{ $client->description }}</td>
                            <td>{{ $client->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

        </div><!--/col-9-->
    </div><!--/row-->
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                {!! $clients->appends(['username' => 'officeinabox'])->render() !!}
            </div>
        </div>
    <style type="text/css">
        body{margin-top:20px;}
    </style>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

</body>
</html>