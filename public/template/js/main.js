var BASE_URL = "http://odyssee.ooredoo.tn";

$(document).ready(function() {

    $("#fullpage").fullpage({
        // verticalCentered: true,
        css3: true,
        sectionsColor: ["rgba(237,28,36,0.3)", "#FFF", "#000"],
        navigation: true,
        navigationPosition: "right",
        navigationTooltips: ["Accueil", "Caractéristique", "Offre"],
        anchors:            ["accueil", "caracteristique", "offre"],
        keyboardScrolling: true,
        animateAnchor: true,
        responsive: 900,
        paddingTop: "85px",
        paddingBottom: "50px",
        afterLoad: function(a, b) {},
        afterRender: function() {
            $("#fullpage").animate({'opacity':1},500);
            $(".loader").fadeOut(300);
        },
        onLeave: function(a, b, c) {}
    }),
    

    $("a").mouseenter(function() {
        Modernizr.audio && ($("#beep-one")[0].volume = .2, $("#beep-one")[0].play());
    })

    $('.share-action').click(function(event) {
        var link = $(this).attr('href');
        window.open(link,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
        event.preventDefault();
    });

    $('.video-cover').click(function(event) {
        $(this).parent().html('<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/O7mtRlZsvXg?autoplay=1&rel=0&modestbranding=1&autohide=1&showinfo=0" frameborder="0" allowfullscreen></iframe>');
        $(this).delete();
    });
});


(function() {
  var countUp, setCount;

  $.getJSON("http://urls.api.twitter.com/1/urls/count.json?url=" + BASE_URL + "&callback=?", function(json) {
    return setCount($("#twitter-count"), json.count);
  });

  $.getJSON("http://graph.facebook.com/" + BASE_URL, function(json) {
    return setCount($("#facebook-count"), json.shares);
  });

  countUp = function($item) {
    return setTimeout(function() {
      var current, newCount, target;
      current = $item.attr("data-current-count") * 1;
      target = $item.attr("data-target-count") * 1;
      newCount = current + Math.ceil((target - current) / 2);
      $item.attr("data-current-count", newCount);
      $item.html(newCount);
      if (newCount < target) {
        return countUp($item);
      }
    }, 100);
  };

  setCount = function($item, count) {
    if (count == null) {
      count = null;
    }
    $item.attr("data-target-count", count);
    $item.attr("data-current-count", 0);
    return countUp($item);
  };

}).call(this);

