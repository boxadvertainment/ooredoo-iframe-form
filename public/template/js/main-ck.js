var flash, phone;

$(document).ready(function() {
    function e() {
        FB.login(function(e) {
            $("body").css("cursor", "default"), e.authResponse ? ($.ajax({
                url: f + "/video.php",
                type: "POST"
            }).done(function() {}), t()) : $("#start-btn").html("Commencer");
        }, {
            scope: "public_profile,email"
        });
    }
    function t() {
        FB.api("me/?fields=name,gender,picture.width(200).height(200)", function(e) {
            var t = {
                id: "video",
                align: "t",
                "class": "myclass"
            }, n = {
                userName: e.name,
                userID: e.id
            }, r = {
                menu: "false",
                allowscriptaccess: "always",
                wmode: "opaque",
                salign: "t"
            };
            userGender = void 0 != e.gender ? e.gender : "agender", swfobject.embedSWF("video_" + userGender + "_noWin.swf?s=1", "videoFlash", "100%", "100%", "9.0.0", "expressInstall.swf", n, r, t), $("#video").addClass("modify");
        });
    }
    function n(e) {
        $("#fbuid").val(e), $("#updateInfo-modal").modal(), $.ajax({
            url: f + "/video.php",
            type: "POST",
            data: {
                experience: !0
            }
        });
    }
    function r() {
        cin = $("#cin").val(), a = $("#tel").val();
        var e = new RegExp("^[0-9]{8}$", "g"), t = new RegExp("^[0-9]{8}$", "g");
        e.test(cin) && t.test(a) ? ($(".validate-update").prop("disabled", !0), $.ajax({
            url: f + "/video.php",
            type: "POST",
            data: {
                cin: cin,
                tel: a
            }
        }).done(function() {
            $(".validate-update").prop("disabled", !1), $("#form_er").html(""), $("#section0 .layer").after('<div id="videoFlash" class="bg"><img src="img/poster.jpg" alt=""></div>'), $("#section0 .layer .description, #start-btn").hide(), $("#section0 .layer .success, #share-btn").show(), $("#updateInfo-modal").modal("hide"), $("#video").hide();
        })) : $("#form_er").html('<div class="alert alert-warning alert-dismissible" id="form_er" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Oups!</strong> Saisie incorrecte, Merci de vérifier</div>');
    }
    function s() {
        FB.ui({
            method: "share",
            href: f
        }, function() {});
    }
    function o() {
        $(".loading").fadeIn(500);
        var e = -1 != navigator.appName.indexOf("Microsoft");
        flash = e ? document.all.video : document.getElementById("video"), FB.api("/" + profileAlbumId + "/photos", function(e) {
            u(e), $("#photos-modal").modal();
        });
    }
    function u(e) {
        $(".modal-body .load-more").remove();
        for (i in e.data) $("<img>", {
            "class": "img-thumbnail",
            alt: e.data[i].name,
            src: e.data[i].source,
            "data-source": e.data[i].source,
            click: function() {
                $(this).addClass("active").siblings().removeClass("active"), $(".validate-btn").prop("disabled", !1);
            }
        }).appendTo("#photos-modal .modal-body");
        "undefined" != typeof e.paging.next && $("<button>", {
            "class": "btn btn-default btn-sm load-more",
            type: "button",
            text: "Load More",
            click: function() {
                $(this).text("Loading ..."), $.get(e.paging.next, u);
            }
        }).appendTo("#photos-modal .modal-body"), $(".loading").fadeOut(500);
    }
    var f = "http://odyssee.ooredoo.tn";
    $("#fullpage").fullpage({
        verticalCentered: !0,
        css3: !0,
        sectionsColor: [ "#000", "#FFF", "#000", "#000", "#000" ],
        navigation: !0,
        resize: !0,
        navigationPosition: "right",
        navigationTooltips: [ "L'expérience", "Odyssée S330", "Odyssée S320", "Odyssée T720", "Pack Odyssée" ],
        anchors: [ "experience", "odyssee_S330", "odyssee_S320", "odyssee_T720", "pack_odyssee" ],
        keyboardScrolling: !0,
        touchSensitivity: 15,
        animateAnchor: !0,
        paddingTop: "85px",
        paddingBottom: "50px",
        normalScrollElements: "#photos-modal,.loading, .modal",
        afterLoad: function(e, t) {
            1 == t || 5 == t || 4 == t || 3 == t ? ($("#fp-nav span").css({
                background: "transparent"
            }), $("#fp-nav .active span").css({
                background: "#FFF"
            })) : ($("#fp-nav span").css({
                background: "transparent"
            }), $("#fp-nav .active span").css({
                background: "#000"
            }));
        },
        afterRender: function() {
            $(".loading").fadeOut(500), $("#fp-nav span").css("border-color", "#fff"), $("#fp-nav li .active span").css("background", "#fff"), $("video").get(0).play();
        },
        onLeave: function(e, t, n) {
            5 == e && "down" == n ? $(".section").eq(e - 1).removeClass("moveDown").addClass("moveUp") : 5 == e && "up" == n && $(".section").eq(e - 1).removeClass("moveUp").addClass("moveDown"), $("#staticImg").toggleClass("active", 1 == e && "down" == n || 5 == e && "up" == n), $("#staticImg").toggleClass("moveDown", 2 == t), $("#staticImg").toggleClass("moveUp", 2 == e && "up" == n), 1 == t || 5 == t || 4 == t || 3 == t ? ($("#fp-nav span").css({
                "border-color": "#fff"
            }), $("#fp-nav .active span").css({
                "border-color": "#fff"
            })) : ($("#fp-nav span").css({
                "border-color": "#000"
            }), $("#fp-nav .active span").css({
                "border-color": "#000"
            })), 3 == t || 5 == t || 4 == t ? $(".footer").removeClass("black") : $(".footer").addClass("black");
        }
    }), $("#start-btn").click(function() {
        $(this).html("Chargement..."), $("body").css("cursor", "progress"), e();
    }), window.modalUpdateInfo = n, window.updateInfo = r, window.shareVideo = s, window.showUserAlbum = o, $(".validate-btn").click(function() {
        if (!this.disabled) {
            var e = $(".modal-body img.active").attr("data-source");
            flash.getSelfie(e), $("#photos-modal").modal("hide");
        }
    }), $("#start-btn, a").mouseenter(function() {
        Modernizr.audio && ($("#beep-one")[0].volume = .2, $("#beep-one")[0].play());
    }), phoneRed = $(".phoneRed").ThreeSixty({
        totalFrames: 22,
        endFrame: 54,
        currentFrame: 0,
        framerate: 16,
        drag: !0,
        imgList: ".threesixty_images",
        progress: ".spinner",
        imagePath: "img/s330/red/",
        filePrefix: "IM000",
        ext: ".png",
        navigation: !1,
        disableSpin: !1,
        autoplayDirection: 1,
        responsive: !0,
        onReady: function() {}
    });
    phoneBlue = $(".phoneBlue").ThreeSixty({
        totalFrames: 22,
        endFrame: 72,
        currentFrame: 0,
        framerate: 16,
        drag: !0,
        imgList: ".threesixty_images",
        progress: ".spinner",
        imagePath: "img/s330/blue/",
        filePrefix: "IM000",
        ext: ".png",
        navigation: !1,
        disableSpin: !1,
        autoplayDirection: 1,
        responsive: !0,
        onReady: function() {}
    });
    phoneWhite = $(".phoneWhite").ThreeSixty({
        totalFrames: 22,
        endFrame: 37,
        currentFrame: 0,
        framerate: 16,
        drag: !0,
        imgList: ".threesixty_images",
        progress: ".spinner",
        imagePath: "img/s330/white/",
        filePrefix: "IM000",
        ext: ".png",
        navigation: !1,
        disableSpin: !1,
        autoplayDirection: 1,
        responsive: !0,
        onReady: function() {}
    });
    phoneBlack = $(".phoneBlack").ThreeSixty({
        totalFrames: 22,
        endFrame: 50,
        currentFrame: 0,
        framerate: 16,
        drag: !0,
        imgList: ".threesixty_images",
        progress: ".spinner",
        imagePath: "img/s330/black/",
        filePrefix: "IM000",
        ext: ".png",
        navigation: !1,
        disableSpin: !1,
        autoplayDirection: 1,
        responsive: !0,
        onReady: function() {}
    });
});