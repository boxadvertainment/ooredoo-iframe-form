/**
 * fullPage 2.2.6
 * https://github.com/alvarotrigo/fullPage.js
 * MIT licensed
 *
 * Copyright (C) 2013 alvarotrigo.com - A project by Alvaro Trigo
 */(function(e) {
    e.fn.fullpage = function(t) {
        function d() {
            e("body").append('<div id="fp-nav"><ul></ul></div>');
            h = e("#fp-nav");
            h.css("color", t.navigationColor);
            h.addClass(t.navigationPosition);
            for (var n = 0; n < e(".fp-section").length; n++) {
                var r = "";
                t.anchors.length && (r = t.anchors[n]);
                var i = t.navigationTooltips[n];
                typeof i == "undefined" && (i = "");
                h.find("ul").append('<li data-tooltip="' + i + '"><a href="#' + r + '"><span></span></a></li>');
            }
        }
        function v() {
            e(".fp-section").each(function() {
                var t = e(this).find(".fp-slide");
                t.length ? t.each(function() {
                    U(e(this));
                }) : U(e(this));
            });
            e.isFunction(t.afterRender) && t.afterRender.call(this);
        }
        function y() {
            if (!t.autoScrolling) {
                var n = e(window).scrollTop(), r = e(".fp-section").map(function() {
                    if (e(this).offset().top < n + 100) return e(this);
                }), i = r[r.length - 1];
                if (!i.hasClass("active")) {
                    g = !0;
                    var s = e(".fp-section.active").index(".fp-section") + 1, o = q(i), u = i.data("anchor");
                    i.addClass("active").siblings().removeClass("active");
                    e.isFunction(t.onLeave) && t.onLeave.call(this, s, i.index(".fp-section") + 1, o);
                    e.isFunction(t.afterLoad) && t.afterLoad.call(this, u, i.index(".fp-section") + 1);
                    F(u);
                    j(u, 0);
                    if (t.anchors.length && !a) {
                        l = u;
                        location.hash = u;
                    }
                    clearTimeout(m);
                    m = setTimeout(function() {
                        g = !1;
                    }, 100);
                }
            }
        }
        function b(e) {
            e.find(".fp-slides").length ? scrollable = e.find(".fp-slide.active").find(".fp-scrollable") : scrollable = e.find(".fp-scrollable");
            return scrollable;
        }
        function w(t, n) {
            if (t == "down") var r = "bottom", i = e.fn.fullpage.moveSectionDown; else var r = "top", i = e.fn.fullpage.moveSectionUp;
            if (n.length > 0) {
                if (!I(r, n)) return !0;
                i();
            } else i();
        }
        function N(n) {
            var i = n.originalEvent;
            t.autoScrolling && n.preventDefault();
            if (!C(n.target)) {
                var s = !1, o = e(".fp-section.active"), u = b(o);
                if (!a && !r) {
                    var f = it(i);
                    x = f.y;
                    T = f.x;
                    o.find(".fp-slides").length && Math.abs(S - T) > Math.abs(E - x) ? Math.abs(S - T) > e(window).width() / 100 * t.touchSensitivity && (S > T ? e.fn.fullpage.moveSlideRight() : e.fn.fullpage.moveSlideLeft()) : t.autoScrolling && Math.abs(E - x) > e(window).height() / 100 * t.touchSensitivity && (E > x ? w("down", u) : x > E && w("up", u));
                }
            }
        }
        function C(n, r) {
            r = r || 0;
            var i = e(n).parent();
            return r < t.normalScrollElementTouchThreshold && i.is(t.normalScrollElements) ? !0 : r == t.normalScrollElementTouchThreshold ? !1 : C(i, ++r);
        }
        function k(e) {
            var t = e.originalEvent, n = it(t);
            E = n.y;
            S = n.x;
        }
        function L(n) {
            if (t.autoScrolling) {
                n = window.event || n;
                var r = Math.max(-1, Math.min(1, n.wheelDelta || -n.deltaY || -n.detail)), i = e(".fp-section.active"), s = b(i);
                a || (r < 0 ? w("down", s) : w("up", s));
                return !1;
            }
        }
        function A(n) {
            var i = e(".fp-section.active"), s = i.find(".fp-slides");
            if (!s.length || r) return;
            var o = s.find(".fp-slide.active"), u = null;
            n === "prev" ? u = o.prev(".fp-slide") : u = o.next(".fp-slide");
            if (!u.length) {
                if (!t.loopHorizontal) return;
                n === "prev" ? u = o.siblings(":last") : u = o.siblings(":first");
            }
            r = !0;
            P(s, u);
        }
        function O() {
            e(".fp-slide.active").each(function() {
                st(e(this));
            });
        }
        function M(r, i, s) {
            var o = {}, u, c = r.position();
            if (typeof c == "undefined") return;
            var h = c.top, d = q(r), v = r.data("anchor"), m = r.index(".fp-section"), g = r.find(".fp-slide.active"), y = e(".fp-section.active"), b = y.index(".fp-section") + 1, w = f;
            if (g.length) var E = g.data("anchor"), S = g.index();
            if (t.autoScrolling && t.continuousVertical && typeof s != "undefined" && (!s && d == "up" || s && d == "down")) {
                s ? e(".fp-section.active").before(y.nextAll(".fp-section")) : e(".fp-section.active").after(y.prevAll(".fp-section").get().reverse());
                ot(e(".fp-section.active").position().top);
                O();
                var x = y;
                c = r.position();
                h = c.top;
                d = q(r);
            }
            r.addClass("active").siblings().removeClass("active");
            a = !0;
            typeof v != "undefined" && G(S, E, v);
            if (t.autoScrolling) {
                o.top = -h;
                u = "." + p;
            } else {
                o.scrollTop = h;
                u = "html, body";
            }
            var T = function() {
                if (!x || !x.length) return;
                s ? e(".fp-section:first").before(x) : e(".fp-section:last").after(x);
                ot(e(".fp-section.active").position().top);
                O();
            }, N = function() {
                T();
                e.isFunction(t.afterLoad) && !w && t.afterLoad.call(this, v, m + 1);
                setTimeout(function() {
                    a = !1;
                    e.isFunction(i) && i.call(this);
                }, n);
            };
            e.isFunction(t.onLeave) && !w && t.onLeave.call(this, b, m + 1, d);
            if (t.css3 && t.autoScrolling) {
                var C = "translate3d(0px, -" + h + "px, 0px)";
                V(C, !0);
                setTimeout(function() {
                    N();
                }, t.scrollingSpeed);
            } else e(u).animate(o, t.scrollingSpeed, t.easing, function() {
                N();
            });
            l = v;
            if (t.autoScrolling) {
                F(v);
                j(v, m);
            }
        }
        function _() {
            var e = window.location.hash.replace("#", "").split("/"), t = e[0], n = e[1];
            t && J(t, n);
        }
        function D() {
            if (!g) {
                var e = window.location.hash.replace("#", "").split("/"), t = e[0], n = e[1];
                if (t.length) {
                    var i = typeof l == "undefined", s = typeof l == "undefined" && typeof n == "undefined" && !r;
                    (t && t !== l && !i || s || !r && c != n) && J(t, n);
                }
            }
        }
        function P(n, i) {
            var s = i.position(), o = n.find(".fp-slidesContainer").parent(), u = i.index(), a = n.closest(".fp-section"), l = a.index(".fp-section"), c = a.data("anchor"), h = a.find(".fp-slidesNav"), p = i.data("anchor"), d = f;
            if (t.onSlideLeave) {
                var v = a.find(".fp-slide.active").index(), m = R(v, u);
                d || e.isFunction(t.onSlideLeave) && t.onSlideLeave.call(this, c, l + 1, v, m);
            }
            i.addClass("active").siblings().removeClass("active");
            typeof p == "undefined" && (p = u);
            if (!t.loopHorizontal) {
                a.find(".fp-controlArrow.fp-prev").toggle(u != 0);
                a.find(".fp-controlArrow.fp-next").toggle(!i.is(":last-child"));
            }
            a.hasClass("active") && G(u, p, c);
            var g = function() {
                d || e.isFunction(t.afterSlideLoad) && t.afterSlideLoad.call(this, c, l + 1, p, u);
                r = !1;
            };
            if (t.css3) {
                var y = "translate3d(-" + s.left + "px, 0px, 0px)";
                n.find(".fp-slidesContainer").toggleClass("fp-easing", t.scrollingSpeed > 0).css(ut(y));
                setTimeout(function() {
                    g();
                }, t.scrollingSpeed, t.easing);
            } else o.animate({
                scrollLeft: s.left
            }, t.scrollingSpeed, t.easing, function() {
                g();
            });
            h.find(".active").removeClass("active");
            h.find("li").eq(u).find("a").addClass("active");
        }
        function B(t, n) {
            var r = 400, i = 400;
            if (t < r || n < i) {
                var s = t * 100 / r, o = n * 100 / i, u = Math.min(s, o), a = u.toFixed(2);
                e("body").css("font-size", a + "%");
            } else e("body").css("font-size", "100%");
        }
        function j(n, r) {
            if (t.navigation) {
                e("#fp-nav").find(".active").removeClass("active");
                n ? e("#fp-nav").find('a[href="#' + n + '"]').addClass("active") : e("#fp-nav").find("li").eq(r).find("a").addClass("active");
            }
        }
        function F(n) {
            if (t.menu) {
                e(t.menu).find(".active").removeClass("active");
                e(t.menu).find('[data-menuanchor="' + n + '"]').addClass("active");
            }
        }
        function I(e, t) {
            if (e === "top") return !t.scrollTop();
            if (e === "bottom") return t.scrollTop() + 1 + t.innerHeight() >= t[0].scrollHeight;
        }
        function q(t) {
            var n = e(".fp-section.active").index(".fp-section"), r = t.index(".fp-section");
            return n > r ? "up" : "down";
        }
        function R(e, t) {
            return e == t ? "none" : e > t ? "left" : "right";
        }
        function U(e) {
            e.css("overflow", "hidden");
            var n = e.closest(".fp-section"), r = e.find(".fp-scrollable");
            if (r.length) var i = r.get(0).scrollHeight; else {
                var i = e.get(0).scrollHeight;
                t.verticalCentered && (i = e.find(".fp-tableCell").get(0).scrollHeight);
            }
            var s = u - parseInt(n.css("padding-bottom")) - parseInt(n.css("padding-top"));
            if (i > s) if (r.length) r.css("height", s + "px").parent().css("height", s + "px"); else {
                t.verticalCentered ? e.find(".fp-tableCell").wrapInner('<div class="fp-scrollable" />') : e.wrapInner('<div class="fp-scrollable" />');
                e.find(".fp-scrollable").slimScroll({
                    allowPageScroll: !0,
                    height: s + "px",
                    size: "10px",
                    alwaysVisible: !0
                });
            } else z(e);
            e.css("overflow", "");
        }
        function z(e) {
            e.find(".fp-scrollable").children().first().unwrap().unwrap();
            e.find(".slimScrollBar").remove();
            e.find(".slimScrollRail").remove();
        }
        function W(e) {
            e.addClass("fp-table").wrapInner('<div class="fp-tableCell" style="height:' + X(e) + 'px;" />');
        }
        function X(e) {
            var n = u;
            if (t.paddingTop || t.paddingBottom) {
                var r = e;
                r.hasClass("fp-section") || (r = e.closest(".fp-section"));
                var i = parseInt(r.css("padding-top")) + parseInt(r.css("padding-bottom"));
                n = u - i;
            }
            return n;
        }
        function V(e, t) {
            o.toggleClass("fp-easing", t);
            o.css(ut(e));
        }
        function J(t, n) {
            typeof n == "undefined" && (n = 0);
            if (isNaN(t)) var r = e('[data-anchor="' + t + '"]'); else var r = e(".fp-section").eq(t - 1);
            t !== l && !r.hasClass("active") ? M(r, function() {
                K(r, n);
            }) : K(r, n);
        }
        function K(e, t) {
            if (typeof t != "undefined") {
                var n = e.find(".fp-slides"), r = n.find('[data-anchor="' + t + '"]');
                r.length || (r = n.find(".fp-slide").eq(t));
                r.length && P(n, r);
            }
        }
        function Q(e, n) {
            e.append('<div class="fp-slidesNav"><ul></ul></div>');
            var r = e.find(".fp-slidesNav");
            r.addClass(t.slidesNavPosition);
            for (var i = 0; i < n; i++) r.find("ul").append('<li><a href="#"><span></span></a></li>');
            r.css("margin-left", "-" + r.width() / 2 + "px");
            r.find("li").first().find("a").addClass("active");
        }
        function G(e, n, r) {
            var i = "";
            if (t.anchors.length) if (e) {
                typeof r != "undefined" && (i = r);
                typeof n == "undefined" && (n = e);
                c = n;
                location.hash = i + "/" + n;
            } else if (typeof e != "undefined") {
                c = n;
                location.hash = r;
            } else location.hash = r;
        }
        function Y() {
            var e = document.createElement("p"), t, n = {
                webkitTransform: "-webkit-transform",
                OTransform: "-o-transform",
                msTransform: "-ms-transform",
                MozTransform: "-moz-transform",
                transform: "transform"
            };
            document.body.insertBefore(e, null);
            for (var r in n) if (e.style[r] !== undefined) {
                e.style[r] = "translate3d(1px,1px,1px)";
                t = window.getComputedStyle(e).getPropertyValue(n[r]);
            }
            document.body.removeChild(e);
            return t !== undefined && t.length > 0 && t !== "none";
        }
        function Z() {
            if (document.addEventListener) {
                document.removeEventListener("mousewheel", L, !1);
                document.removeEventListener("wheel", L, !1);
            } else document.detachEvent("onmousewheel", L);
        }
        function et() {
            if (document.addEventListener) {
                document.addEventListener("mousewheel", L, !1);
                document.addEventListener("wheel", L, !1);
            } else document.attachEvent("onmousewheel", L);
        }
        function tt() {
            if (i || s) {
                MSPointer = rt();
                e(document).off("touchstart " + MSPointer.down).on("touchstart " + MSPointer.down, k);
                e(document).off("touchmove " + MSPointer.move).on("touchmove " + MSPointer.move, N);
            }
        }
        function nt() {
            if (i || s) {
                MSPointer = rt();
                e(document).off("touchstart " + MSPointer.down);
                e(document).off("touchmove " + MSPointer.move);
            }
        }
        function rt() {
            var e;
            window.PointerEvent ? e = {
                down: "pointerdown",
                move: "pointermove"
            } : e = {
                down: "MSPointerDown",
                move: "MSPointerMove"
            };
            return e;
        }
        function it(e) {
            var t = new Array;
            if (window.navigator.msPointerEnabled) {
                t.y = e.pageY;
                t.x = e.pageX;
            } else {
                t.y = e.touches[0].pageY;
                t.x = e.touches[0].pageX;
            }
            return t;
        }
        function st(n) {
            var r = t.scrollingSpeed;
            e.fn.fullpage.setScrollingSpeed(0);
            P(n.closest(".fp-slides"), n);
            e.fn.fullpage.setScrollingSpeed(r);
        }
        function ot(e) {
            if (t.css3) {
                var n = "translate3d(0px, -" + e + "px, 0px)";
                V(n, !1);
            } else o.css("top", -e);
        }
        function ut(e) {
            return {
                "-webkit-transform": e,
                "-moz-transform": e,
                "-ms-transform": e,
                transform: e
            };
        }
        function at() {
            ot(0);
            e("#fp-nav, .fp-slidesNav, .fp-controlArrow").remove();
            e(".fp-section").css({
                height: "",
                "background-color": "",
                padding: ""
            });
            e(".fp-slide").css({
                width: ""
            });
            o.css({
                height: "",
                position: "",
                "-ms-touch-action": ""
            });
            e(".fp-section, .fp-slide").each(function() {
                z(e(this));
                e(this).removeClass("fp-table active");
            });
            o.find(".fp-easing").removeClass("fp-easing");
            o.find(".fp-tableCell, .fp-slidesContainer, .fp-slides").each(function() {
                e(this).replaceWith(this.childNodes);
            });
            e("html, body").scrollTop(0);
            o.addClass("fullpage-used");
        }
        t = e.extend({
            verticalCentered: !0,
            resize: !0,
            sectionsColor: [],
            anchors: [],
            scrollingSpeed: 700,
            easing: "easeInQuart",
            menu: !1,
            navigation: !1,
            navigationPosition: "right",
            navigationColor: "#000",
            navigationTooltips: [],
            slidesNavigation: !1,
            slidesNavPosition: "bottom",
            controlArrowColor: "#fff",
            loopBottom: !1,
            loopTop: !1,
            loopHorizontal: !0,
            autoScrolling: !0,
            scrollOverflow: !1,
            css3: !1,
            paddingTop: 0,
            paddingBottom: 0,
            fixedElements: null,
            normalScrollElements: null,
            keyboardScrolling: !0,
            touchSensitivity: 5,
            continuousVertical: !1,
            animateAnchor: !0,
            normalScrollElementTouchThreshold: 5,
            sectionSelector: ".section",
            slideSelector: ".slide",
            afterLoad: null,
            onLeave: null,
            afterRender: null,
            afterResize: null,
            afterSlideLoad: null,
            onSlideLeave: null
        }, t);
        if (t.continuousVertical && (t.loopTop || t.loopBottom)) {
            t.continuousVertical = !1;
            console && console.log && console.log("Option loopTop/loopBottom is mutually exclusive with continuousVertical; continuousVertical disabled");
        }
        var n = 600;
        e.fn.fullpage.setAutoScrolling = function(n) {
            t.autoScrolling = n;
            var r = e(".fp-section.active");
            if (t.autoScrolling) {
                e("html, body").css({
                    overflow: "hidden",
                    height: "100%"
                });
                r.length && ot(r.position().top);
            } else {
                e("html, body").css({
                    overflow: "visible",
                    height: "initial"
                });
                ot(0);
                e("html, body").scrollTop(r.position().top);
            }
        };
        e.fn.fullpage.setScrollingSpeed = function(e) {
            t.scrollingSpeed = e;
        };
        e.fn.fullpage.setMouseWheelScrolling = function(e) {
            e ? et() : Z();
        };
        e.fn.fullpage.setAllowScrolling = function(t) {
            if (t) {
                e.fn.fullpage.setMouseWheelScrolling(!0);
                tt();
            } else {
                e.fn.fullpage.setMouseWheelScrolling(!1);
                nt();
            }
        };
        e.fn.fullpage.setKeyboardScrolling = function(e) {
            t.keyboardScrolling = e;
        };
        var r = !1, i = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|BB10|Windows Phone|Tizen|Bada)/), s = "ontouchstart" in window || navigator.msMaxTouchPoints > 0, o = e(this), u = e(window).height(), a = !1, f = !1, l, c, h, p = "fullpage-wrapper";
        e.fn.fullpage.setAllowScrolling(!0);
        t.css3 && (t.css3 = Y());
        if (e(this).length) {
            o.css({
                height: "100%",
                position: "relative",
                "-ms-touch-action": "none"
            });
            o.addClass(p);
        } else console.error("Error! Fullpage.js needs to be initialized with a selector. For example: $('#myContainer').fullpage();");
        e(t.sectionSelector).each(function() {
            e(this).addClass("fp-section");
        });
        e(t.slideSelector).each(function() {
            e(this).addClass("fp-slide");
        });
        t.navigation && d();
        e(".fp-section").each(function(n) {
            var r = e(this), i = e(this).find(".fp-slide"), s = i.length;
            !n && e(".fp-section.active").length === 0 && e(this).addClass("active");
            e(this).css("height", u + "px");
            (t.paddingTop || t.paddingBottom) && e(this).css("padding", t.paddingTop + " 0 " + t.paddingBottom + " 0");
            typeof t.sectionsColor[n] != "undefined" && e(this).css("background-color", t.sectionsColor[n]);
            typeof t.anchors[n] != "undefined" && e(this).attr("data-anchor", t.anchors[n]);
            if (s > 1) {
                var o = s * 100, a = 100 / s;
                i.wrapAll('<div class="fp-slidesContainer" />');
                i.parent().wrap('<div class="fp-slides" />');
                e(this).find(".fp-slidesContainer").css("width", o + "%");
                e(this).find(".fp-slides").after('<div class="fp-controlArrow fp-prev"></div><div class="fp-controlArrow fp-next"></div>');
                if (t.controlArrowColor != "#fff") {
                    e(this).find(".fp-controlArrow.fp-next").css("border-color", "transparent transparent transparent " + t.controlArrowColor);
                    e(this).find(".fp-controlArrow.fp-prev").css("border-color", "transparent " + t.controlArrowColor + " transparent transparent");
                }
                t.loopHorizontal || e(this).find(".fp-controlArrow.fp-prev").hide();
                t.slidesNavigation && Q(e(this), s);
                i.each(function(n) {
                    e(this).css("width", a + "%");
                    t.verticalCentered && W(e(this));
                });
                var f = r.find(".fp-slide.active");
                f.length == 0 ? i.eq(0).addClass("active") : st(f);
            } else t.verticalCentered && W(e(this));
        }).promise().done(function() {
            e.fn.fullpage.setAutoScrolling(t.autoScrolling);
            var n = e(".fp-section.active").find(".fp-slide.active");
            n.length && (e(".fp-section.active").index(".fp-section") != 0 || e(".fp-section.active").index(".fp-section") == 0 && n.index() != 0) && st(n);
            t.fixedElements && t.css3 && e(t.fixedElements).appendTo("body");
            if (t.navigation) {
                h.css("margin-top", "-" + h.height() / 2 + "px");
                h.find("li").eq(e(".fp-section.active").index(".fp-section")).find("a").addClass("active");
            }
            t.menu && t.css3 && e(t.menu).closest(".fullpage-wrapper").length && e(t.menu).appendTo("body");
            if (t.scrollOverflow) {
                o.hasClass("fullpage-used") && v();
                e(window).on("load", v);
            } else e.isFunction(t.afterRender) && t.afterRender.call(this);
            var r = window.location.hash.replace("#", "").split("/"), i = r[0];
            if (i.length) {
                var s = e('[data-anchor="' + i + '"]');
                if (!t.animateAnchor && s.length) {
                    if (t.autoScrolling) ot(s.position().top); else {
                        ot(0);
                        e("html, body").scrollTop(s.position().top);
                    }
                    F(i);
                    j(i, null);
                    e.isFunction(t.afterLoad) && t.afterLoad.call(this, i, s.index(".fp-section") + 1);
                    s.addClass("active").siblings().removeClass("active");
                }
            }
            e(window).on("load", function() {
                _();
            });
        });
        var m, g = !1;
        e(window).on("scroll", y);
        var E = 0, S = 0, x = 0, T = 0;
        e.fn.fullpage.moveSectionUp = function() {
            var n = e(".fp-section.active").prev(".fp-section");
            !n.length && (t.loopTop || t.continuousVertical) && (n = e(".fp-section").last());
            n.length && M(n, null, !0);
        };
        e.fn.fullpage.moveSectionDown = function() {
            var n = e(".fp-section.active").next(".fp-section");
            !n.length && (t.loopBottom || t.continuousVertical) && (n = e(".fp-section").first());
            n.length && M(n, null, !1);
        };
        e.fn.fullpage.moveTo = function(t, n) {
            var r = "";
            isNaN(t) ? r = e('[data-anchor="' + t + '"]') : r = e(".fp-section").eq(t - 1);
            typeof n != "undefined" ? J(t, n) : r.length > 0 && M(r);
        };
        e.fn.fullpage.moveSlideRight = function() {
            A("next");
        };
        e.fn.fullpage.moveSlideLeft = function() {
            A("prev");
        };
        e(window).on("hashchange", D);
        e(document).keydown(function(n) {
            if (t.keyboardScrolling && !a) switch (n.which) {
              case 38:
              case 33:
                e.fn.fullpage.moveSectionUp();
                break;
              case 40:
              case 34:
                e.fn.fullpage.moveSectionDown();
                break;
              case 36:
                e.fn.fullpage.moveTo(1);
                break;
              case 35:
                e.fn.fullpage.moveTo(e(".fp-section").length);
                break;
              case 37:
                e.fn.fullpage.moveSlideLeft();
                break;
              case 39:
                e.fn.fullpage.moveSlideRight();
                break;
              default:
                return;
            }
        });
        e(document).on("click", "#fp-nav a", function(t) {
            t.preventDefault();
            var n = e(this).parent().index();
            M(e(".fp-section").eq(n));
        });
        e(document).on({
            mouseenter: function() {
                var n = e(this).data("tooltip");
                e('<div class="fp-tooltip ' + t.navigationPosition + '">' + n + "</div>").hide().appendTo(e(this)).fadeIn(200);
            },
            mouseleave: function() {
                e(this).find(".fp-tooltip").fadeOut(200, function() {
                    e(this).remove();
                });
            }
        }, "#fp-nav li");
        if (t.normalScrollElements) {
            e(document).on("mouseenter", t.normalScrollElements, function() {
                e.fn.fullpage.setMouseWheelScrolling(!1);
            });
            e(document).on("mouseleave", t.normalScrollElements, function() {
                e.fn.fullpage.setMouseWheelScrolling(!0);
            });
        }
        e(".fp-section").on("click", ".fp-controlArrow", function() {
            e(this).hasClass("fp-prev") ? e.fn.fullpage.moveSlideLeft() : e.fn.fullpage.moveSlideRight();
        });
        e(".fp-section").on("click", ".toSlide", function(t) {
            t.preventDefault();
            var n = e(this).closest(".fp-section").find(".fp-slides"), r = n.find(".fp-slide.active"), i = null;
            i = n.find(".fp-slide").eq(e(this).data("index") - 1);
            i.length > 0 && P(n, i);
        });
        var H;
        e(window).resize(function() {
            if (i) e.fn.fullpage.reBuild(); else {
                clearTimeout(H);
                H = setTimeout(e.fn.fullpage.reBuild, 500);
            }
        });
        e.fn.fullpage.reBuild = function() {
            f = !0;
            var n = e(window).width();
            u = e(window).height();
            t.resize && B(u, n);
            e(".fp-section").each(function() {
                var n = u - parseInt(e(this).css("padding-bottom")) - parseInt(e(this).css("padding-top"));
                t.verticalCentered && e(this).find(".fp-tableCell").css("height", X(e(this)) + "px");
                e(this).css("height", u + "px");
                if (t.scrollOverflow) {
                    var r = e(this).find(".fp-slide");
                    r.length ? r.each(function() {
                        U(e(this));
                    }) : U(e(this));
                }
                var r = e(this).find(".fp-slides");
                r.length && P(r, r.find(".fp-slide.active"));
            });
            var r = e(".fp-section.active").position(), i = e(".fp-section.active");
            i.index(".fp-section") && M(i);
            f = !1;
            e.isFunction(t.afterResize) && t.afterResize.call(this);
        };
        e(document).on("click", ".fp-slidesNav a", function(t) {
            t.preventDefault();
            var n = e(this).closest(".fp-section").find(".fp-slides"), r = n.find(".fp-slide").eq(e(this).closest("li").index());
            P(n, r);
        });
        e.fn.fullpage.destroy = function(n) {
            e.fn.fullpage.setAutoScrolling(!1);
            e.fn.fullpage.setAllowScrolling(!1);
            e.fn.fullpage.setKeyboardScrolling(!1);
            e(window).off("scroll", y).off("hashchange", D);
            e(document).off("click", "#fp-nav a").off("mouseenter", "#fp-nav li").off("mouseleave", "#fp-nav li").off("click", ".fp-slidesNav a").off("mouseover", t.normalScrollElements).off("mouseout", t.normalScrollElements);
            e(".fp-section").off("click", ".fp-controlArrow").off("click", ".toSlide");
            n && at();
        };
    };
})(jQuery);