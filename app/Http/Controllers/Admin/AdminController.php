<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller {

    public function dashboard(Request $request)
    {
        if ($request->input('username') != 'officeinabox') {
            return response('Unauthorized.', 401);
        }

        $clients = User::paginate(15);
        $count = User::count();

        return view('admin.dashboard', ['clients' => $clients, 'count' => $count]);
    }
}
