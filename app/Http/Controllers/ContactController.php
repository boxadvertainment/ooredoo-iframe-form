<?php namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use Mail;
use Config;

class ContactController extends Controller {

	public function getIndex()
	{

		return view('contact');
	}

	public function postIndex(ContactFormRequest $request)
	{

        Mail::send('emails.contact', [
            'msg'     => $request->input('message'),
            'name'    => $request->input('name'),
            'email'   => $request->input('email'),
            'subject' => $request->input('subject')
        ], function($message) use($request)
        {
            $message->from($request->input('email'), $request->input('name'));
            $message->to(Config::get('app.email'), Config::get('app.name'))->subject(Config::get('app.name') . ' - Contact Form');
        });

        return redirect()->route('contact')->with('success', true);
	}

}
