<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Session;

class AppController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function home()
    {
        return view('home');
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^[0-9]{8}$/',
            'demande' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $user = new User;
        $user->name = $request->input('name');
        $user->company = $request->input('company');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->offre = $request->input('offre');
        $user->source = $request->input('source');
        $user->demande  = $request->input('demande');
        $user->description = $request->input('description');

        if ($user->save()) {
            return redirect()->action('AppController@home')->with('message', 'asf');
        }
        return response()->json(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.']);
    }

}
