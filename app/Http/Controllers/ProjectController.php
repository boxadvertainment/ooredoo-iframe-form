<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller {

	public function index()
	{
		$projects = Project::where('status', 'approved')->paginate()->get();
		return view('project.index', ['projects' => $projects]);
	}

	public function show($id)
	{
		$project = Project::find($id);
		return view('project.show', ['project' => $project]);
	}

	public function like($id)
	{
		$rules = array(
			'id' => 'exists:projects|unique:users_like_projects,name_id,NULL,id,user_id,' . Auth::id()
		);

		$validator = Validator::make(array('id' => $id), $rules);
		if ($validator->fails()) {
			$messages = $validator->messages()->all();
			return Response::json(array('success' => false, 'messages' => $messages));
		}

		DB::table('users_like_names')->insert(array(
			'name_id'   => $id,
			'user_id'    => Auth::id(),
			'ip'         => Request::getClientIp(),
			'created_at' => (new DateTime())->format('Y-m-d H:i:s')
		));

		return Response::json(array('success' => true));
	}

	public function search($q)
	{

	}

}
